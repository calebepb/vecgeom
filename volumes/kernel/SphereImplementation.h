
/// @file SphereImplementation.h
/// @author Raman Sehgal (raman.sehgal@cern.ch)

#ifndef VECGEOM_VOLUMES_KERNEL_SPHEREIMPLEMENTATION_H_
#define VECGEOM_VOLUMES_KERNEL_SPHEREIMPLEMENTATION_H_
#include "base/Global.h"

#include "base/Transformation3D.h"
#include "volumes/UnplacedSphere.h"
#include "base/Vector3D.h"
#include "volumes/kernel/GenericKernels.h"

#include <cstdio>

namespace vecgeom {

VECGEOM_DEVICE_DECLARE_CONV_TEMPLATE_2v(struct, SphereImplementation, TranslationCode, translation::kGeneric,
                                        RotationCode, rotation::kGeneric);

inline namespace VECGEOM_IMPL_NAMESPACE {

class PlacedSphere;

template <TranslationCode transCodeT, RotationCode rotCodeT>
struct SphereImplementation {

  static const int transC = transCodeT;
  static const int rotC   = rotCodeT;

  using PlacedShape_t   = PlacedSphere;
  using UnplacedShape_t = UnplacedSphere;

  VECGEOM_CUDA_HEADER_BOTH
  static void PrintType() { printf("SpecializedSphere<%i, %i>", transCodeT, rotCodeT); }

  template <typename Stream>
  static void PrintType(Stream &s)
  {
    s << "SpecializedSphere<" << transCodeT << "," << rotCodeT << ","
      << ">";
  }

  template <typename Stream>
  static void PrintImplementationType(Stream &s)
  {
    s << "SphereImplementation<" << transCodeT << "," << rotCodeT << ">";
  }

  template <typename Stream>
  static void PrintUnplacedType(Stream &s)
  {
    s << "UnplacedSphere";
  }

  // Some New Helper functions
  template <class Backend>
  VECGEOM_CUDA_HEADER_BOTH
  static typename Backend::bool_v IsPointOnInnerRadius(UnplacedSphere const &unplaced,
                                                       Vector3D<typename Backend::precision_v> const &point)
  {

    Precision innerRad2 = unplaced.GetInnerRadius() * unplaced.GetInnerRadius();
    return ((point.Mag2() >= (innerRad2 - kTolerance)) && (point.Mag2() <= (innerRad2 + kTolerance)));
  }

  template <class Backend>
  VECGEOM_CUDA_HEADER_BOTH
  static typename Backend::bool_v IsPointOnOuterRadius(UnplacedSphere const &unplaced,
                                                       Vector3D<typename Backend::precision_v> const &point)
  {

    Precision outerRad2 = unplaced.GetOuterRadius() * unplaced.GetOuterRadius();
    return ((point.Mag2() >= (outerRad2 - kTolerance)) && (point.Mag2() <= (outerRad2 + kTolerance)));
  }

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static typename Backend::bool_v IsPointOnStartPhi(UnplacedSphere const &unplaced,
                                                    Vector3D<typename Backend::precision_v> const &point)
  {

    return unplaced.GetWedge().IsOnSurfaceGeneric<Backend>(unplaced.GetWedge().GetAlong1(),
                                                           unplaced.GetWedge().GetNormal1(), point);
  }

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static typename Backend::bool_v IsPointOnEndPhi(UnplacedSphere const &unplaced,
                                                  Vector3D<typename Backend::precision_v> const &point)
  {

    return unplaced.GetWedge().IsOnSurfaceGeneric<Backend>(unplaced.GetWedge().GetAlong2(),
                                                           unplaced.GetWedge().GetNormal2(), point);
  }

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static typename Backend::bool_v IsPointOnStartTheta(UnplacedSphere const &unplaced,
                                                      Vector3D<typename Backend::precision_v> const &point)
  {

    return unplaced.GetThetaCone().IsOnSurfaceGeneric<Backend, true>(point);
  }

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static typename Backend::bool_v IsPointOnEndTheta(UnplacedSphere const &unplaced,
                                                    Vector3D<typename Backend::precision_v> const &point)
  {

    return unplaced.GetThetaCone().IsOnSurfaceGeneric<Backend, false>(point);
  }

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void UnplacedContains(UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &point,
                               typename Backend::bool_v &inside);

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void Contains(UnplacedSphere const &unplaced, Transformation3D const &transformation,
                       Vector3D<typename Backend::precision_v> const &point,
                       Vector3D<typename Backend::precision_v> &localPoint, typename Backend::bool_v &inside);

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void Inside(UnplacedSphere const &unplaced, Transformation3D const &transformation,
                     Vector3D<typename Backend::precision_v> const &point, typename Backend::inside_v &inside);

  template <typename Backend, bool ForInside>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void GenericKernelForContainsAndInside(UnplacedSphere const &unplaced,
                                                Vector3D<typename Backend::precision_v> const &localPoint,
                                                typename Backend::bool_v &completelyinside,
                                                typename Backend::bool_v &completelyoutside);

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void DistanceToIn(UnplacedSphere const &unplaced, Transformation3D const &transformation,
                           Vector3D<typename Backend::precision_v> const &point,
                           Vector3D<typename Backend::precision_v> const &direction,
                           typename Backend::precision_v const &stepMax, typename Backend::precision_v &distance);

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void DistanceToOut(UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &point,
                            Vector3D<typename Backend::precision_v> const &direction,
                            typename Backend::precision_v const &stepMax, typename Backend::precision_v &distance);

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void DistanceToOutKernel(UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &point,
                                  Vector3D<typename Backend::precision_v> const &direction,
                                  typename Backend::precision_v const &stepMax,
                                  typename Backend::precision_v &distance);

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void SafetyToIn(UnplacedSphere const &unplaced, Transformation3D const &transformation,
                         Vector3D<typename Backend::precision_v> const &point, typename Backend::precision_v &safety);

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void SafetyToOut(UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &point,
                          typename Backend::precision_v &safety);

  template <typename Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void ContainsKernel(UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &localPoint,
                             typename Backend::bool_v &inside);

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void InsideKernel(UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &point,
                           typename Backend::inside_v &inside);

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void DistanceToInKernel(UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &point,
                                 Vector3D<typename Backend::precision_v> const &direction,
                                 typename Backend::precision_v const &stepMax, typename Backend::precision_v &distance);

  template <class Backend, bool DistToIn>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void GetMinDistFromPhi(UnplacedSphere const &unplaced,
                                Vector3D<typename Backend::precision_v> const &localPoint,
                                Vector3D<typename Backend::precision_v> const &localDir, typename Backend::bool_v &done,
                                typename Backend::precision_v &distance);

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void SafetyToInKernel(UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &point,
                               typename Backend::precision_v &safety);

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void SafetyToOutKernel(UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &point,
                                typename Backend::precision_v &safety);

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void Normal(UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &point,
                     Vector3D<typename Backend::precision_v> &normal, typename Backend::bool_v &valid);

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void NormalKernel(UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &point,
                           Vector3D<typename Backend::precision_v> &normal, typename Backend::bool_v &valid);

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static Vector3D<typename Backend::precision_v> ApproxSurfaceNormalKernel(
      UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &point);

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static typename Backend::bool_v IsCompletelyOutside(UnplacedSphere const &unplaced,
                                                      Vector3D<typename Backend::precision_v> const &localPoint)
  {

    typedef typename Backend::precision_v Float_t;
    typedef typename Backend::bool_v Bool_t;
    Float_t rad               = localPoint.Mag();
    Precision fRmax           = unplaced.GetOuterRadius();
    Precision fRmin           = unplaced.GetInnerRadius();
    Bool_t outsideRadiusRange = (rad > (fRmax + kTolerance)) || (rad < (fRmin - kTolerance));
    Bool_t outsidePhiRange(false), insidePhiRange(false);
    unplaced.GetWedge().GenericKernelForContainsAndInside<Backend, true>(localPoint, insidePhiRange, outsidePhiRange);
    Bool_t outsideThetaRange = unplaced.GetThetaCone().IsCompletelyOutside<Backend>(localPoint);
    Bool_t completelyoutside = outsideRadiusRange || outsidePhiRange || outsideThetaRange;
    return completelyoutside;
  }

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static typename Backend::bool_v IsCompletelyInside(UnplacedSphere const &unplaced,
                                                     Vector3D<typename Backend::precision_v> const &localPoint)
  {

    typedef typename Backend::precision_v Float_t;
    typedef typename Backend::bool_v Bool_t;
    Float_t rad              = localPoint.Mag();
    Precision fRmax          = unplaced.GetOuterRadius();
    Precision fRmin          = unplaced.GetInnerRadius();
    Bool_t insideRadiusRange = (rad < (fRmax - kTolerance)) && (rad > (fRmin + kTolerance));
    Bool_t outsidePhiRange(false), insidePhiRange(false);
    unplaced.GetWedge().GenericKernelForContainsAndInside<Backend, true>(localPoint, insidePhiRange, outsidePhiRange);
    Bool_t insideThetaRange = unplaced.GetThetaCone().IsCompletelyInside<Backend>(localPoint);
    Bool_t completelyinside = insideRadiusRange && insidePhiRange && insideThetaRange;
    return completelyinside;
  }

  template <class Backend, bool ForInnerRadius, bool MovingOut>
  VECGEOM_CUDA_HEADER_BOTH
  static typename Backend::bool_v IsPointOnRadialSurfaceAndMovingOut(
      UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &point,
      Vector3D<typename Backend::precision_v> const &dir)
  {

    if (MovingOut) {
      if (ForInnerRadius) {
        return IsPointOnInnerRadius<Backend>(unplaced, point) && (dir.Dot(-point) > 0.);
      } else {
        return IsPointOnOuterRadius<Backend>(unplaced, point) && (dir.Dot(point) > 0.);
      }
    } else {
      if (ForInnerRadius) {
        return IsPointOnInnerRadius<Backend>(unplaced, point) && (dir.Dot(-point) < 0.);
      } else
        return IsPointOnOuterRadius<Backend>(unplaced, point) && (dir.Dot(point) < 0.);
    }
  }

  template <class Backend, bool MovingOut>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static typename Backend::bool_v IsPointOnSurfaceAndMovingOut(UnplacedSphere const &unplaced,
                                                               Vector3D<typename Backend::precision_v> const &point,
                                                               Vector3D<typename Backend::precision_v> const &dir)
  {

    typedef typename Backend::bool_v Bool_t;

    Bool_t tempOuterRad = IsPointOnRadialSurfaceAndMovingOut<Backend, false, MovingOut>(unplaced, point, dir);
    Bool_t tempInnerRad(false), tempStartPhi(false), tempEndPhi(false), tempStartTheta(false), tempEndTheta(false);
    if (unplaced.GetInnerRadius())
      tempInnerRad = IsPointOnRadialSurfaceAndMovingOut<Backend, true, MovingOut>(unplaced, point, dir);
    if (unplaced.GetDeltaPhiAngle() < (kTwoPi - kHalfTolerance)) {
      tempStartPhi = unplaced.GetWedge().IsPointOnSurfaceAndMovingOut<Backend, true, MovingOut>(point, dir);
      tempEndPhi   = unplaced.GetWedge().IsPointOnSurfaceAndMovingOut<Backend, false, MovingOut>(point, dir);
    }
    if (unplaced.GetDeltaThetaAngle() < (kPi - kHalfTolerance)) {
      tempStartTheta = unplaced.GetThetaCone().IsPointOnSurfaceAndMovingOut<Backend, true, MovingOut>(point, dir);
      tempEndTheta   = unplaced.GetThetaCone().IsPointOnSurfaceAndMovingOut<Backend, false, MovingOut>(point, dir);
    }

    Bool_t isPointOnSurfaceAndMovingOut =
        ((tempOuterRad || tempInnerRad) && unplaced.GetWedge().Contains<Backend>(point) &&
         unplaced.GetThetaCone().Contains<Backend>(point)) ||
        ((tempStartPhi || tempEndPhi) && (point.Mag2() >= unplaced.GetInnerRadius() * unplaced.GetInnerRadius()) &&
         (point.Mag2() <= unplaced.GetOuterRadius() * unplaced.GetOuterRadius()) &&
         unplaced.GetThetaCone().Contains<Backend>(point)) ||
        ((tempStartTheta || tempEndTheta) && (point.Mag2() >= unplaced.GetInnerRadius() * unplaced.GetInnerRadius()) &&
         (point.Mag2() <= unplaced.GetOuterRadius() * unplaced.GetOuterRadius()) &&
         unplaced.GetWedge().Contains<Backend>(point));

    return isPointOnSurfaceAndMovingOut;
  }
};

template <TranslationCode transCodeT, RotationCode rotCodeT>
template <typename Backend>
VECGEOM_CUDA_HEADER_BOTH
void SphereImplementation<transCodeT, rotCodeT>::Normal(UnplacedSphere const &unplaced,
                                                        Vector3D<typename Backend::precision_v> const &point,
                                                        Vector3D<typename Backend::precision_v> &normal,
                                                        typename Backend::bool_v &valid)
{

  NormalKernel<Backend>(unplaced, point, normal, valid);
}

/* This function should be called from NormalKernel, only for the
 * cases when the point is not on the surface and one want to calculate
 * the SurfaceNormal.
 *
 * Algo : Find the boundary which is closest to the point,
 * and return the normal to that boundary.
 *
 */
template <TranslationCode transCodeT, RotationCode rotCodeT>
template <typename Backend>
VECGEOM_CUDA_HEADER_BOTH
Vector3D<typename Backend::precision_v> SphereImplementation<transCodeT, rotCodeT>::ApproxSurfaceNormalKernel(
    UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &point)
{

  typedef typename Backend::precision_v Float_t;

  Vector3D<Float_t> norm(0., 0., 0.);
  Float_t radius   = point.Mag();
  Float_t distRMax = Abs(radius - unplaced.GetOuterRadius());
  Float_t distRMin = Abs(unplaced.GetInnerRadius() - radius);
  vecCore::MaskedAssign(distRMax, distRMax < 0.0, InfinityLength<Float_t>());
  vecCore::MaskedAssign(distRMin, distRMin < 0.0, InfinityLength<Float_t>());
  Float_t distMin = Min(distRMin, distRMax);

  Float_t distPhi1 =
      point.x() * unplaced.GetWedge().GetNormal1().x() + point.y() * unplaced.GetWedge().GetNormal1().y();
  Float_t distPhi2 =
      point.x() * unplaced.GetWedge().GetNormal2().x() + point.y() * unplaced.GetWedge().GetNormal2().y();
  vecCore::MaskedAssign(distPhi1, distPhi1 < 0.0, InfinityLength<Float_t>());
  vecCore::MaskedAssign(distPhi2, distPhi2 < 0.0, InfinityLength<Float_t>());
  distMin = Min(distMin, Min(distPhi1, distPhi2));

  Float_t rho = point.Perp();
  Float_t distTheta1 =
      unplaced.GetThetaCone().DistanceToLine<Backend>(unplaced.GetThetaCone().GetSlope1(), rho, point.z());
  Float_t distTheta2 =
      unplaced.GetThetaCone().DistanceToLine<Backend>(unplaced.GetThetaCone().GetSlope2(), rho, point.z());
  vecCore::MaskedAssign(distTheta1, distTheta1 < 0.0, InfinityLength<Float_t>());
  vecCore::MaskedAssign(distTheta2, distTheta2 < 0.0, InfinityLength<Float_t>());
  distMin = Min(distMin, Min(distTheta1, distTheta2));

  vecCore::MaskedAssign(norm, distMin == distRMax, point.Unit());
  vecCore::MaskedAssign(norm, distMin == distRMin, -point.Unit());

  Vector3D<Float_t> normal1 = unplaced.GetWedge().GetNormal1();
  Vector3D<Float_t> normal2 = unplaced.GetWedge().GetNormal2();
  vecCore::MaskedAssign(norm, distMin == distPhi1, -normal1);
  vecCore::MaskedAssign(norm, distMin == distPhi2, -normal2);

  vecCore::MaskedAssign(norm, distMin == distTheta1, norm + unplaced.GetThetaCone().GetNormal1<Backend>(point));
  vecCore::MaskedAssign(norm, distMin == distTheta2, norm + unplaced.GetThetaCone().GetNormal2<Backend>(point));

  return norm;
}

template <TranslationCode transCodeT, RotationCode rotCodeT>
template <typename Backend>
VECGEOM_CUDA_HEADER_BOTH
void SphereImplementation<transCodeT, rotCodeT>::NormalKernel(UnplacedSphere const &unplaced,
                                                              Vector3D<typename Backend::precision_v> const &point,
                                                              Vector3D<typename Backend::precision_v> &normal,
                                                              typename Backend::bool_v &valid)
{

  normal.Set(0.);
  typedef typename Backend::precision_v Float_t;
  typedef typename Backend::bool_v Bool_t;

  /* Assumption : This function assumes that the point is on the surface.
   *
   * Algorithm :
   * Detect all those surfaces on which the point is at, and count the
   * numOfSurfaces. if(numOfSurfaces == 1) then normal corresponds to the
   * normal for that particular case.
   *
   * if(numOfSurfaces > 1 ), then add the normals corresponds to different
   * cases, and finally normalize it and return.
   *
   * We need following function
   * IsPointOnInnerRadius()
   * IsPointOnOuterRadius()
   * IsPointOnStartPhi()
   * IsPointOnEndPhi()
   * IsPointOnStartTheta()
   * IsPointOnEndTheta()
   *
   * set valid=true if numOfSurface > 0
   *
   * if above mentioned assumption not followed , ie.
   * In case the given point is outside, then find the closest boundary,
   * the required normal will be the normal to that boundary.
   * This logic is implemented in "ApproxSurfaceNormalKernel" function
   */

  Bool_t isPointOutside(false);

  // May be required Later
  /*
  if (!ForDistanceToOut) {
    Bool_t unused(false);
    GenericKernelForContainsAndInside<Backend, true>(unplaced, point, unused, isPointOutside);
    MaskedAssign(unused || isPointOutside, ApproxSurfaceNormalKernel<Backend>(unplaced, point), &normal);
  }
  */

  Bool_t isPointInside(false);
  GenericKernelForContainsAndInside<Backend, true>(unplaced, point, isPointInside, isPointOutside);
  vecCore::MaskedAssign(normal, isPointInside || isPointOutside, ApproxSurfaceNormalKernel<Backend>(unplaced, point));

  valid = Bool_t(false);

  Float_t noSurfaces(0.);
  Bool_t isPointOnOuterRadius = IsPointOnOuterRadius<Backend>(unplaced, point);

  vecCore::MaskedAssign(noSurfaces, isPointOnOuterRadius, noSurfaces + 1);
  vecCore::MaskedAssign(normal, !isPointOutside && isPointOnOuterRadius, normal + (point.Unit()));

  if (unplaced.GetInnerRadius()) {
    Bool_t isPointOnInnerRadius = IsPointOnInnerRadius<Backend>(unplaced, point);
    vecCore::MaskedAssign(noSurfaces, isPointOnInnerRadius, noSurfaces + 1);
    vecCore::MaskedAssign(normal, !isPointOutside && isPointOnInnerRadius, normal - point.Unit());
  }

  if (!unplaced.IsFullPhiSphere()) {
    Bool_t isPointOnStartPhi = IsPointOnStartPhi<Backend>(unplaced, point);
    Bool_t isPointOnEndPhi   = IsPointOnEndPhi<Backend>(unplaced, point);
    vecCore::MaskedAssign(noSurfaces, isPointOnStartPhi, noSurfaces + 1);
    vecCore::MaskedAssign(noSurfaces, isPointOnEndPhi, noSurfaces + 1);
    vecCore::MaskedAssign(normal, !isPointOutside && isPointOnStartPhi, normal - unplaced.GetWedge().GetNormal1());
    vecCore::MaskedAssign(normal, !isPointOutside && isPointOnEndPhi, normal - unplaced.GetWedge().GetNormal2());
  }

  if (!unplaced.IsFullThetaSphere()) {
    Bool_t isPointOnStartTheta = IsPointOnStartTheta<Backend>(unplaced, point);
    Bool_t isPointOnEndTheta   = IsPointOnEndTheta<Backend>(unplaced, point);

    vecCore::MaskedAssign(noSurfaces, isPointOnStartTheta, noSurfaces + 1);
    vecCore::MaskedAssign(normal, !isPointOutside && isPointOnStartTheta,
                          normal + unplaced.GetThetaCone().GetNormal1<Backend>(point));

    vecCore::MaskedAssign(noSurfaces, isPointOnEndTheta, noSurfaces + 1);
    vecCore::MaskedAssign(normal, !isPointOutside && isPointOnEndTheta,
                          normal + unplaced.GetThetaCone().GetNormal2<Backend>(point));

    Vector3D<Float_t> tempNormal(0., 0., -1.);
    vecCore::MaskedAssign(normal, !isPointOutside && isPointOnStartTheta && isPointOnEndTheta &&
                                      (unplaced.GetETheta() <= kPi / 2.),
                          tempNormal);
    Vector3D<Float_t> tempNormal2(0., 0., 1.);
    vecCore::MaskedAssign(normal, !isPointOutside && isPointOnStartTheta && isPointOnEndTheta &&
                                      (unplaced.GetSTheta() >= kPi / 2.),
                          tempNormal2);
  }

  normal = normal.Unit();

  valid = (noSurfaces > 0.);
}

template <TranslationCode transCodeT, RotationCode rotCodeT>
template <typename Backend>
VECGEOM_CUDA_HEADER_BOTH
void SphereImplementation<transCodeT, rotCodeT>::Contains(UnplacedSphere const &unplaced,
                                                          Transformation3D const &transformation,
                                                          Vector3D<typename Backend::precision_v> const &point,
                                                          Vector3D<typename Backend::precision_v> &localPoint,
                                                          typename Backend::bool_v &inside)
{

  localPoint = transformation.Transform<transCodeT, rotCodeT>(point);
  UnplacedContains<Backend>(unplaced, localPoint, inside);
}

template <TranslationCode transCodeT, RotationCode rotCodeT>
template <typename Backend>
VECGEOM_CUDA_HEADER_BOTH
void SphereImplementation<transCodeT, rotCodeT>::UnplacedContains(UnplacedSphere const &unplaced,
                                                                  Vector3D<typename Backend::precision_v> const &point,
                                                                  typename Backend::bool_v &inside)
{

  ContainsKernel<Backend>(unplaced, point, inside);
}

template <TranslationCode transCodeT, RotationCode rotCodeT>
template <typename Backend>
VECGEOM_CUDA_HEADER_BOTH
void SphereImplementation<transCodeT, rotCodeT>::ContainsKernel(
    UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &localPoint,
    typename Backend::bool_v &inside)
{

  typedef typename Backend::bool_v Bool_t;
  Bool_t unused;
  Bool_t outside;
  GenericKernelForContainsAndInside<Backend, false>(unplaced, localPoint, unused, outside);
  inside = !outside;
}

template <TranslationCode transCodeT, RotationCode rotCodeT>
template <typename Backend, bool ForInside>
VECGEOM_CUDA_HEADER_BOTH
void SphereImplementation<transCodeT, rotCodeT>::GenericKernelForContainsAndInside(
    UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &localPoint,
    typename Backend::bool_v &completelyinside, typename Backend::bool_v &completelyoutside)
{

  typedef typename Backend::precision_v Float_t;
  typedef typename Backend::bool_v Bool_t;

  Precision fRmin          = unplaced.GetInnerRadius();
  Precision fRminTolerance = unplaced.GetFRminTolerance();
  Precision fRmax          = unplaced.GetOuterRadius();

  Float_t rad2 = localPoint.Mag2();
  Float_t tolRMin(fRmin + (fRminTolerance * 10. * 2));
  Float_t tolRMax(fRmax - (unplaced.GetMKTolerance() * 10. * 2));

  // Check radial surfaces
  // Radial check for GenericKernel Start
  if (unplaced.GetInnerRadius())
    completelyinside = (rad2 <= tolRMax * tolRMax) && (rad2 >= tolRMin * tolRMin);
  else
    completelyinside = (rad2 <= tolRMax * tolRMax);
  // std::cout<<"Comp In - Rad : "<<completelyinside<<std::endl;

  tolRMin = fRmin - (0.5 * fRminTolerance * 10 * 2);
  tolRMax = fRmax + (0.5 * unplaced.GetMKTolerance() * 10 * 2);
  if (unplaced.GetInnerRadius())
    completelyoutside = (rad2 <= tolRMin * tolRMin) || (rad2 >= tolRMax * tolRMax);
  else
    completelyoutside = (rad2 >= tolRMax * tolRMax);

  // Phi boundaries  : Do not check if it has no phi boundary!
  if (!unplaced.IsFullPhiSphere()) {

    Bool_t completelyoutsidephi;
    Bool_t completelyinsidephi;
    unplaced.GetWedge().GenericKernelForContainsAndInside<Backend, ForInside>(localPoint, completelyinsidephi,
                                                                              completelyoutsidephi);
    completelyoutside |= completelyoutsidephi;

    if (ForInside) completelyinside &= completelyinsidephi;
  }
  // Phi Check for GenericKernel Over

  // Theta bondaries
  if (!unplaced.IsFullThetaSphere()) {

    Bool_t completelyoutsidetheta(false);
    Bool_t completelyinsidetheta(false);
    unplaced.GetThetaCone().GenericKernelForContainsAndInside<Backend, ForInside>(localPoint, completelyinsidetheta,
                                                                                  completelyoutsidetheta);
    completelyoutside |= completelyoutsidetheta;

    if (ForInside) completelyinside &= completelyinsidetheta;
  }
  return;
}

template <TranslationCode transCodeT, RotationCode rotCodeT>
template <typename Backend>
VECGEOM_CUDA_HEADER_BOTH
void SphereImplementation<transCodeT, rotCodeT>::Inside(UnplacedSphere const &unplaced,
                                                        Transformation3D const &transformation,
                                                        Vector3D<typename Backend::precision_v> const &point,
                                                        typename Backend::inside_v &inside)
{

  InsideKernel<Backend>(unplaced, transformation.Transform<transCodeT, rotCodeT>(point), inside);
}

template <TranslationCode transCodeT, RotationCode rotCodeT>
template <class Backend>
VECGEOM_CUDA_HEADER_BOTH
void SphereImplementation<transCodeT, rotCodeT>::InsideKernel(UnplacedSphere const &unplaced,
                                                              Vector3D<typename Backend::precision_v> const &point,
                                                              typename Backend::inside_v &inside)
{
  // use double-based vector for result, as bool_v is a mask for precision_v
  const typename Backend::precision_v in(EInside::kInside);
  const typename Backend::precision_v out(EInside::kOutside);
  typename Backend::bool_v inmask(false), outmask(false);
  typename Backend::precision_v result(EInside::kSurface);

  GenericKernelForContainsAndInside<Backend, true>(unplaced, point, inmask, outmask);

  vecCore::MaskedAssign(result, inmask, in);
  vecCore::MaskedAssign(result, outmask, out);

  // Manual conversion from double to int here is necessary because int_v and
  // precision_v have different number of elements in SIMD vector, so bool_v
  // (mask for precision_v) cannot be cast to mask for inside, which is a
  // different type and does not exist in the current backend system
  for (size_t i = 0; i < vecCore::VectorSize(result); i++)
    vecCore::Set(inside, i, vecCore::Get(result, i));
}

template <TranslationCode transCodeT, RotationCode rotCodeT>
template <class Backend>
VECGEOM_FORCE_INLINE
VECGEOM_CUDA_HEADER_BOTH
void SphereImplementation<transCodeT, rotCodeT>::SafetyToIn(UnplacedSphere const &unplaced,
                                                            Transformation3D const &transformation,
                                                            Vector3D<typename Backend::precision_v> const &point,
                                                            typename Backend::precision_v &safety)
{

  SafetyToInKernel<Backend>(unplaced, transformation.Transform<transCodeT, rotCodeT>(point), safety);
}

template <TranslationCode transCodeT, RotationCode rotCodeT>
template <class Backend>
VECGEOM_CUDA_HEADER_BOTH
void SphereImplementation<transCodeT, rotCodeT>::SafetyToInKernel(UnplacedSphere const &unplaced,
                                                                  Vector3D<typename Backend::precision_v> const &point,
                                                                  typename Backend::precision_v &safety)
{

  typedef typename Backend::precision_v Float_t;
  typedef typename Backend::bool_v Bool_t;

  Bool_t done(false);

  // General Precalcs
  Float_t rad = point.Mag();

  // Distance to r shells
  Precision fRmin = unplaced.GetInnerRadius();
  Float_t fRminV(fRmin);
  Precision fRmax = unplaced.GetOuterRadius();
  Float_t fRmaxV(fRmax);
  Float_t safeRMin(0.);
  Float_t safeRMax(0.);

  Bool_t completelyinside(false), completelyoutside(false);
  GenericKernelForContainsAndInside<Backend, true>(unplaced, point, completelyinside, completelyoutside);

  vecCore::MaskedAssign(safety, completelyinside, Float_t(-1.0));
  done |= completelyinside;
  if (vecCore::MaskFull(done)) return;

  Bool_t isOnSurface = !completelyinside && !completelyoutside;
  vecCore::MaskedAssign(safety, !done && isOnSurface, Float_t(0.0));
  done |= isOnSurface;
  if (vecCore::MaskFull(done)) return;

  if (fRmin) {
    safeRMin = fRminV - rad;
    safeRMax = rad - fRmaxV;
    safety   = vecCore::Blend(!done && (safeRMin > safeRMax), safeRMin, safeRMax);
  } else {
    vecCore::MaskedAssign(safety, !done, (rad - fRmaxV));
  }
  // Distance to r shells over

  // Distance to phi extent
  if (!unplaced.IsFullPhiSphere()) {
    Float_t safetyPhi = unplaced.GetWedge().SafetyToIn<Backend>(point);
    vecCore::MaskedAssign(safety, !done, Max(safetyPhi, safety));
  }

  // Distance to Theta extent
  if (!unplaced.IsFullThetaSphere()) {
    Float_t safetyTheta = unplaced.GetThetaCone().SafetyToIn<Backend>(point);
    vecCore::MaskedAssign(safety, !done, Max(safetyTheta, safety));
  }
}

template <TranslationCode transCodeT, RotationCode rotCodeT>
template <class Backend>
VECGEOM_FORCE_INLINE
VECGEOM_CUDA_HEADER_BOTH
void SphereImplementation<transCodeT, rotCodeT>::SafetyToOut(UnplacedSphere const &unplaced,
                                                             Vector3D<typename Backend::precision_v> const &point,
                                                             typename Backend::precision_v &safety)
{
  SafetyToOutKernel<Backend>(unplaced, point, safety);
}

template <TranslationCode transCodeT, RotationCode rotCodeT>
template <class Backend>
VECGEOM_CUDA_HEADER_BOTH
void SphereImplementation<transCodeT, rotCodeT>::SafetyToOutKernel(UnplacedSphere const &unplaced,
                                                                   Vector3D<typename Backend::precision_v> const &point,
                                                                   typename Backend::precision_v &safety)
{

  typedef typename Backend::precision_v Float_t;
  typedef typename Backend::bool_v Bool_t;

  Float_t rad = point.Mag();

  Bool_t done(false);

  // Distance to r shells
  Precision fRmin = unplaced.GetInnerRadius();
  Float_t fRminV(fRmin);
  Precision fRmax = unplaced.GetOuterRadius();
  Float_t fRmaxV(fRmax);

  Bool_t completelyinside(false), completelyoutside(false);
  GenericKernelForContainsAndInside<Backend, true>(unplaced, point, completelyinside, completelyoutside);
  vecCore::MaskedAssign(safety, completelyoutside, Float_t(-1.0));
  done |= completelyoutside;
  if (vecCore::MaskFull(done)) return;

  Bool_t isOnSurface = !completelyinside && !completelyoutside;
  vecCore::MaskedAssign(safety, !done && isOnSurface, Float_t(0.0));
  done |= isOnSurface;
  if (vecCore::MaskFull(done)) return;

  // Distance to r shells
  if (fRmin) {
    Float_t safeRMin = (rad - fRminV);
    Float_t safeRMax = (fRmaxV - rad);
    safety           = vecCore::Blend(!done && (safeRMin < safeRMax), safeRMin, safeRMax);
  } else {
    vecCore::MaskedAssign(safety, !done, (fRmaxV - rad));
  }

  // Distance to phi extent
  if (!unplaced.IsFullPhiSphere()) {
    Float_t safetyPhi = unplaced.GetWedge().SafetyToOut<Backend>(point);
    vecCore::MaskedAssign(safety, !done, Min(safetyPhi, safety));
  }

  // Distance to Theta extent
  Float_t safeTheta(0.);
  if (!unplaced.IsFullThetaSphere()) {
    safeTheta = unplaced.GetThetaCone().SafetyToOut<Backend>(point);
    vecCore::MaskedAssign(safety, !done, Min(safeTheta, safety));
  }
}

template <TranslationCode transCodeT, RotationCode rotCodeT>
template <class Backend>
VECGEOM_CUDA_HEADER_BOTH
void SphereImplementation<transCodeT, rotCodeT>::DistanceToIn(UnplacedSphere const &unplaced,
                                                              Transformation3D const &transformation,
                                                              Vector3D<typename Backend::precision_v> const &point,
                                                              Vector3D<typename Backend::precision_v> const &direction,
                                                              typename Backend::precision_v const &stepMax,
                                                              typename Backend::precision_v &distance)
{

  DistanceToInKernel<Backend>(unplaced, transformation.Transform<transCodeT, rotCodeT>(point),
                              transformation.TransformDirection<rotCodeT>(direction), stepMax, distance);
}

template <TranslationCode transCodeT, RotationCode rotCodeT>
template <class Backend>
VECGEOM_CUDA_HEADER_BOTH
void SphereImplementation<transCodeT, rotCodeT>::DistanceToInKernel(
    UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &point,
    Vector3D<typename Backend::precision_v> const &direction, typename Backend::precision_v const & /*stepMax*/,
    typename Backend::precision_v &distance)
{

  typedef typename Backend::precision_v Float_t;
  typedef typename Backend::bool_v Bool_t;

  distance = kInfLength;

  Bool_t done(false);

  Float_t fRmax(unplaced.GetOuterRadius());
  Float_t fRmin(unplaced.GetInnerRadius());

  bool fullPhiSphere   = unplaced.IsFullPhiSphere();
  bool fullThetaSphere = unplaced.IsFullThetaSphere();

  Vector3D<Float_t> tmpPt;
  // General Precalcs
  Float_t rad2    = point.Mag2();
  Float_t pDotV3d = point.Dot(direction);

  Float_t c = rad2 - fRmax * fRmax;

  Bool_t cond = IsCompletelyInside<Backend>(unplaced, point);
  vecCore::MaskedAssign(distance, cond, Float_t(-1.0));
  done |= cond;
  if (vecCore::MaskFull(done)) return;

  cond = IsPointOnSurfaceAndMovingOut<Backend, false>(unplaced, point, direction);
  vecCore::MaskedAssign(distance, !done && cond, Float_t(0.0));
  done |= cond;
  if (vecCore::MaskFull(done)) return;

  Float_t sd1(kInfLength);
  Float_t sd2(kInfLength);
  Float_t d2 = (pDotV3d * pDotV3d - c);
  cond       = (d2 < 0. || ((c > 0.0) && (pDotV3d > 0.0)));
  done |= cond;
  if (vecCore::MaskFull(done)) return; // Returning in case of no intersection with outer shell

  // Note: Abs(d2) was introduced to avoid Sqrt(negative) in other lanes than the ones satisfying d2>=0.
  vecCore::MaskedAssign(sd1, d2 >= 0.0, (-pDotV3d - Sqrt(Abs(d2))));

  Float_t outerDist(kInfLength);
  Float_t innerDist(kInfLength);

  if (unplaced.IsFullSphere()) {
    vecCore::MaskedAssign(outerDist, !done && (sd1 >= 0.0), sd1);
  } else {
    tmpPt = point + sd1 * direction;
    vecCore::MaskedAssign(outerDist, !done && unplaced.GetWedge().Contains<Backend>(tmpPt) &&
                                         unplaced.GetThetaCone().Contains<Backend>(tmpPt) && (sd1 >= 0.),
                          sd1);
  }

  if (unplaced.GetInnerRadius()) {
    c  = rad2 - fRmin * fRmin;
    d2 = pDotV3d * pDotV3d - c;
    // Note: Abs(d2) was introduced to avoid Sqrt(negative) in other lanes than the ones satisfying d2>=0.
    vecCore::MaskedAssign(sd2, d2 >= 0.0, (-pDotV3d + Sqrt(Abs(d2))));

    if (unplaced.IsFullSphere()) {
      vecCore::MaskedAssign(innerDist, !done && (sd2 >= 0.0), sd2);
    } else {
      //   std::cout<<" ---- Called by InnerRad ---- " << std::endl;
      tmpPt = point + sd2 * direction;
      vecCore::MaskedAssign(innerDist, !done && (sd2 >= 0.) && unplaced.GetWedge().Contains<Backend>(tmpPt) &&
                                           unplaced.GetThetaCone().Contains<Backend>(tmpPt),
                            sd2);
    }
  }

  distance = Min(outerDist, innerDist);

  if (!fullPhiSphere) {
    GetMinDistFromPhi<Backend, true>(unplaced, point, direction, done, distance);
  }

  Float_t distThetaMin(kInfLength);

  if (!fullThetaSphere) {
    Bool_t intsect1(false);
    Bool_t intsect2(false);
    Float_t distTheta1(kInfLength);
    Float_t distTheta2(kInfLength);

    unplaced.GetThetaCone().DistanceToIn<Backend>(point, direction, distTheta1, distTheta2, intsect1,
                                                  intsect2); //,cone1IntSecPt, cone2IntSecPt);
    Vector3D<Float_t> coneIntSecPt1 = point + distTheta1 * direction;
    Float_t distCone1               = coneIntSecPt1.Mag2();

    Vector3D<Float_t> coneIntSecPt2 = point + distTheta2 * direction;
    Float_t distCone2               = coneIntSecPt2.Mag2();

    Bool_t isValidCone1 = (distCone1 >= fRmin * fRmin && distCone1 <= fRmax * fRmax) && intsect1;
    Bool_t isValidCone2 = (distCone2 >= fRmin * fRmin && distCone2 <= fRmax * fRmax) && intsect2;

    if (!fullPhiSphere) {
      isValidCone1 &= unplaced.GetWedge().Contains<Backend>(coneIntSecPt1);
      isValidCone2 &= unplaced.GetWedge().Contains<Backend>(coneIntSecPt2);
    }
    vecCore::MaskedAssign(distThetaMin, (!done && isValidCone2 && !isValidCone1), distTheta2);
    vecCore::MaskedAssign(distThetaMin, (!done && isValidCone1 && !isValidCone2), distTheta1);
    vecCore::MaskedAssign(distThetaMin, (!done && isValidCone1 && isValidCone2), Min(distTheta1, distTheta2));
  }

  distance = Min(distThetaMin, distance);

  Vector3D<Float_t> directDir = (Vector3D<Float_t>(0., 0., 0.) - point);
  Float_t newDist             = directDir.Mag();
  vecCore::MaskedAssign(
      distance, Bool_t(unplaced.GetSTheta() > kHalfTolerance || unplaced.GetETheta() < (kPi - kHalfTolerance)) &&
                    (Abs(directDir.Unit().x() - direction.x()) < kHalfTolerance) &&
                    (Abs(directDir.Unit().y() - direction.y()) < kHalfTolerance) &&
                    (Abs(directDir.Unit().z() - direction.z()) < kHalfTolerance),
      Min(distance, newDist));
}

template <TranslationCode transCodeT, RotationCode rotCodeT>
template <class Backend, bool DistToIn>
VECGEOM_CUDA_HEADER_BOTH
void SphereImplementation<transCodeT, rotCodeT>::GetMinDistFromPhi(
    UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &localPoint,
    Vector3D<typename Backend::precision_v> const &localDir, typename Backend::bool_v &done,
    typename Backend::precision_v &distance)
{

  typedef typename Backend::precision_v Float_t;
  typedef typename Backend::bool_v Bool_t;
  Float_t distPhi1(kInfLength);
  Float_t distPhi2(kInfLength);
  Float_t dist(kInfLength);

  if (DistToIn)
    unplaced.GetWedge().DistanceToIn<Backend>(localPoint, localDir, distPhi1, distPhi2);
  else
    unplaced.GetWedge().DistanceToOut<Backend>(localPoint, localDir, distPhi1, distPhi2);

  Vector3D<Float_t> tmpPt;
  Bool_t containsCond1(false), containsCond2(false);
  // Min Face
  dist  = Min(distPhi1, distPhi2);
  tmpPt = localPoint + dist * localDir;

  Precision fRmax = unplaced.GetOuterRadius();
  Precision fRmin = unplaced.GetInnerRadius();
  Float_t rad2    = tmpPt.Mag2();

  Bool_t tempCond(false);
  tempCond = ((dist == distPhi1) && unplaced.GetWedge().IsOnSurfaceGeneric<Backend, true>(tmpPt)) ||
             ((dist == distPhi2) && unplaced.GetWedge().IsOnSurfaceGeneric<Backend, false>(tmpPt));

  containsCond1 =
      tempCond && (rad2 > fRmin * fRmin) && (rad2 < fRmax * fRmax) && unplaced.GetThetaCone().Contains<Backend>(tmpPt);

  vecCore::MaskedAssign(distance, !done && containsCond1, Min(dist, distance));

  // Max Face
  dist  = Max(distPhi1, distPhi2);
  tmpPt = localPoint + dist * localDir;

  rad2     = tmpPt.Mag2();
  tempCond = Bool_t(false);
  tempCond = ((dist == distPhi1) && unplaced.GetWedge().IsOnSurfaceGeneric<Backend, true>(tmpPt)) ||
             ((dist == distPhi2) && unplaced.GetWedge().IsOnSurfaceGeneric<Backend, false>(tmpPt));

  containsCond2 =
      tempCond && (rad2 > fRmin * fRmin) && (rad2 < fRmax * fRmax) && unplaced.GetThetaCone().Contains<Backend>(tmpPt);
  vecCore::MaskedAssign(distance, ((!done) && (!containsCond1) && containsCond2), Min(dist, distance));
}

template <TranslationCode transCodeT, RotationCode rotCodeT>
template <class Backend>
VECGEOM_CUDA_HEADER_BOTH
void SphereImplementation<transCodeT, rotCodeT>::DistanceToOut(UnplacedSphere const &unplaced,
                                                               Vector3D<typename Backend::precision_v> const &point,
                                                               Vector3D<typename Backend::precision_v> const &direction,
                                                               typename Backend::precision_v const &stepMax,
                                                               typename Backend::precision_v &distance)
{

  DistanceToOutKernel<Backend>(unplaced, point, direction, stepMax, distance);
}

// V3
template <TranslationCode transCodeT, RotationCode rotCodeT>
template <class Backend>
VECGEOM_CUDA_HEADER_BOTH
void SphereImplementation<transCodeT, rotCodeT>::DistanceToOutKernel(
    UnplacedSphere const &unplaced, Vector3D<typename Backend::precision_v> const &point,
    Vector3D<typename Backend::precision_v> const &direction, typename Backend::precision_v const & /*stepMax*/,
    typename Backend::precision_v &distance)
{

  typedef typename Backend::precision_v Float_t;
  typedef typename Backend::bool_v Bool_t;

  distance = kInfLength;

  Bool_t done(false);
  Float_t snxt(kInfLength);

  Float_t fRmax(unplaced.GetOuterRadius());
  Float_t fRmin(unplaced.GetInnerRadius());

  // Intersection point
  Vector3D<Float_t> intSecPt;
  Float_t d2(0.);

  Float_t pDotV3d = point.Dot(direction);

  Float_t rad2 = point.Mag2();
  Float_t c    = rad2 - fRmax * fRmax;

  Float_t sd1(kInfLength);
  Float_t sd2(kInfLength);

  Bool_t cond = IsCompletelyOutside<Backend>(unplaced, point);
  vecCore::MaskedAssign(distance, cond, Float_t(-1.0));

  done |= cond;
  if (vecCore::MaskFull(done)) return;

  cond = IsPointOnSurfaceAndMovingOut<Backend, true>(unplaced, point, direction);
  vecCore::MaskedAssign(distance, !done && cond, Float_t(0.0));
  done |= cond;
  if (vecCore::MaskFull(done)) return;

  // Note: Abs(d2) was introduced to avoid Sqrt(negative) in other lanes than the ones satisfying d2>=0.
  d2 = (pDotV3d * pDotV3d - c);
  vecCore::MaskedAssign(sd1, (!done && (d2 >= 0.0)), (-pDotV3d + Sqrt(Abs(d2))));

  if (unplaced.GetInnerRadius()) {
    c  = rad2 - fRmin * fRmin;
    d2 = (pDotV3d * pDotV3d - c);
    vecCore::MaskedAssign(sd2, (!done && (d2 >= 0.0) && (pDotV3d < 0.0)), (-pDotV3d - Sqrt(Abs(d2))));
  }

  snxt = Min(sd1, sd2);

  Bool_t condSemi = (Bool_t(unplaced.GetSTheta() == 0. && unplaced.GetETheta() == kPi / 2.) && direction.z() >= 0.) ||
                    (Bool_t(unplaced.GetSTheta() == kPi / 2. && unplaced.GetETheta() == kPi) && direction.z() <= 0.);
  vecCore::MaskedAssign(distance, !done && condSemi, snxt);
  done |= condSemi;
  if (vecCore::MaskFull(done)) return;

  Float_t distThetaMin(kInfLength);
  Float_t distPhiMin(kInfLength);

  if (!unplaced.IsFullThetaSphere()) {
    Bool_t intsect1(false);
    Bool_t intsect2(false);
    Float_t distTheta1(kInfLength);
    Float_t distTheta2(kInfLength);
    unplaced.GetThetaCone().DistanceToOut<Backend>(point, direction, distTheta1, distTheta2, intsect1, intsect2);
    vecCore::MaskedAssign(distThetaMin, (intsect2 && !intsect1), distTheta2);
    vecCore::MaskedAssign(distThetaMin, (!intsect2 && intsect1), distTheta1);
    vecCore::MaskedAssign(distThetaMin, (intsect2 && intsect1), Min(distTheta1, distTheta2));
  }

  distance = Min(distThetaMin, snxt);

  if (!unplaced.IsFullPhiSphere()) {
    if (unplaced.GetDeltaPhiAngle() <= kPi) {
      Float_t distPhi1;
      Float_t distPhi2;
      unplaced.GetWedge().DistanceToOut<Backend>(point, direction, distPhi1, distPhi2);
      distPhiMin = Min(distPhi1, distPhi2);
      distance   = Min(distPhiMin, distance);
    } else {
      GetMinDistFromPhi<Backend, false>(unplaced, point, direction, done, distance);
    }
  }
}
}
} // End global namespace

#endif // VECGEOM_VOLUMES_KERNEL_SPHEREIMPLEMENTATION_H_
