/// @file TorusImplementation.h

#ifndef VECGEOM_VOLUMES_KERNEL_TORUSIMPLEMENTATION2_H_
#define VECGEOM_VOLUMES_KERNEL_TORUSIMPLEMENTATION2_H_

#include "base/Global.h"
#include "base/Transformation3D.h"
#include "volumes/kernel/GenericKernels.h"
#include "volumes/kernel/TubeImplementation.h"
#include "volumes/UnplacedTorus2.h"

#include <cstdio>

namespace vecgeom {

VECGEOM_DEVICE_DECLARE_CONV_TEMPLATE_2v(struct, TorusImplementation2, TranslationCode, translation::kGeneric,
                                        RotationCode, rotation::kGeneric);

inline namespace VECGEOM_IMPL_NAMESPACE {

//_____________________________________________________________________________
template <typename T>
VECGEOM_CUDA_HEADER_BOTH
unsigned int SolveCubic(T a, T b, T c, T *x)
{
  // Find real solutions of the cubic equation : x^3 + a*x^2 + b*x + c = 0
  // Input: a,b,c
  // Output: x[3] real solutions
  // Returns number of real solutions (1 or 3)
  const T ott        = 1. / 3.;
  const T sq3        = Sqrt(3.);
  const T inv6sq3    = 1. / (6. * sq3);
  unsigned int ireal = 1;
  T p                = b - a * a * ott;
  T q                = c - a * b * ott + 2. * a * a * a * ott * ott * ott;
  T delta            = 4 * p * p * p + 27. * q * q;
  T t, u;
  if (delta >= 0) {
    delta = Sqrt(delta);
    t     = (-3 * q * sq3 + delta) * inv6sq3;
    u     = (3 * q * sq3 + delta) * inv6sq3;
    x[0]  = CopySign(1., t) * Cbrt(Abs(t)) - CopySign(1., u) * Cbrt(Abs(u)) - a * ott;
  } else {
    delta = Sqrt(-delta);
    t     = -0.5 * q;
    u     = delta * inv6sq3;
    x[0]  = 2. * Pow(t * t + u * u, 0.5 * ott) * cos(ott * ATan2(u, t));
    x[0] -= a * ott;
  }

  t     = x[0] * x[0] + a * x[0] + b;
  u     = a + x[0];
  delta = u * u - 4. * t;
  if (delta >= 0) {
    ireal = 3;
    delta = Sqrt(delta);
    x[1]  = 0.5 * (-u - delta);
    x[2]  = 0.5 * (-u + delta);
  }
  return ireal;
}

template <typename T, unsigned int i, unsigned int j>
VECGEOM_FORCE_INLINE
VECGEOM_CUDA_HEADER_BOTH
void CmpAndSwap(T *array)
{
  if (array[i] > array[j]) {
    T c      = array[j];
    array[j] = array[i];
    array[i] = c;
  }
}

// a special function to sort a 4 element array
// sorting is done inplace and in increasing order
// implementation comes from a sorting network
template <typename T>
VECGEOM_FORCE_INLINE
VECGEOM_CUDA_HEADER_BOTH
void Sort4(T *array)
{
  CmpAndSwap<T, 0, 2>(array);
  CmpAndSwap<T, 1, 3>(array);
  CmpAndSwap<T, 0, 1>(array);
  CmpAndSwap<T, 2, 3>(array);
  CmpAndSwap<T, 1, 2>(array);
}

// solve quartic taken from ROOT/TGeo and adapted
//_____________________________________________________________________________

template <typename T>
VECGEOM_CUDA_HEADER_BOTH
int SolveQuartic(T a, T b, T c, T d, T *x)
{
  // Find real solutions of the quartic equation : x^4 + a*x^3 + b*x^2 + c*x + d = 0
  // Input: a,b,c,d
  // Output: x[4] - real solutions
  // Returns number of real solutions (0 to 3)
  T e     = b - 3. * a * a / 8.;
  T f     = c + a * a * a / 8. - 0.5 * a * b;
  T g     = d - 3. * a * a * a * a / 256. + a * a * b / 16. - a * c / 4.;
  T xx[4] = {vecgeom::kInfLength, vecgeom::kInfLength, vecgeom::kInfLength, vecgeom::kInfLength};
  T delta;
  T h                = 0.;
  unsigned int ireal = 0;

  // special case when f is zero
  if (Abs(f) < 1E-6) {
    delta = e * e - 4. * g;
    if (delta < 0) return 0;
    delta = Sqrt(delta);
    h     = 0.5 * (-e - delta);
    if (h >= 0) {
      h          = Sqrt(h);
      x[ireal++] = -h - 0.25 * a;
      x[ireal++] = h - 0.25 * a;
    }
    h = 0.5 * (-e + delta);
    if (h >= 0) {
      h          = Sqrt(h);
      x[ireal++] = -h - 0.25 * a;
      x[ireal++] = h - 0.25 * a;
    }
    Sort4(x);
    return ireal;
  }

  if (Abs(g) < 1E-6) {
    x[ireal++] = -0.25 * a;
    // this actually wants to solve a second order equation
    // we should specialize if it happens often
    unsigned int ncubicroots = SolveCubic<T>(0, e, f, xx);
    // this loop is not nice
    for (unsigned int i = 0; i < ncubicroots; i++)
      x[ireal++]        = xx[i] - 0.25 * a;
    Sort4(x); // could be Sort3
    return ireal;
  }

  ireal = SolveCubic<T>(2. * e, e * e - 4. * g, -f * f, xx);
  if (ireal == 1) {
    if (xx[0] <= 0) return 0;
    h = Sqrt(xx[0]);
  } else {
    // 3 real solutions of the cubic
    for (unsigned int i = 0; i < 3; i++) {
      h = xx[i];
      if (h >= 0) break;
    }
    if (h <= 0) return 0;
    h = Sqrt(h);
  }
  T j   = 0.5 * (e + h * h - f / h);
  ireal = 0;
  delta = h * h - 4. * j;
  if (delta >= 0) {
    delta      = Sqrt(delta);
    x[ireal++] = 0.5 * (-h - delta) - 0.25 * a;
    x[ireal++] = 0.5 * (-h + delta) - 0.25 * a;
  }
  delta = h * h - 4. * g / j;
  if (delta >= 0) {
    delta      = Sqrt(delta);
    x[ireal++] = 0.5 * (h - delta) - 0.25 * a;
    x[ireal++] = 0.5 * (h + delta) - 0.25 * a;
  }
  Sort4(x);
  return ireal;
}

class PlacedTorus2;

template <TranslationCode transCodeT, RotationCode rotCodeT>
struct TorusImplementation2 {

  static const int transC = transCodeT;
  static const int rotC   = rotCodeT;

  using PlacedShape_t   = PlacedTorus2;
  using UnplacedShape_t = UnplacedTorus2;

  VECGEOM_CUDA_HEADER_BOTH
  static void PrintType() { printf("SpecializedTorus2<%i, %i>", transCodeT, rotCodeT); }

  template <typename Stream>
  static void PrintType(Stream &s)
  {
    s << "SpecializedTorus2<" << transCodeT << "," << rotCodeT << ","
      << ">";
  }

  template <typename Stream>
  static void PrintImplementationType(Stream &s)
  {
    s << "TorusImplemenation2<" << transCodeT << "," << rotCodeT << ">";
  }

  template <typename Stream>
  static void PrintUnplacedType(Stream &s)
  {
    s << "UnplacedTorus2";
  }

  /////GenericKernel Contains/Inside implementation
  template <typename Backend, bool ForInside, bool notForDisk>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void GenericKernelForContainsAndInside(UnplacedTorus2 const &torus,
                                                Vector3D<typename Backend::precision_v> const &point,
                                                typename Backend::bool_v &completelyinside,
                                                typename Backend::bool_v &completelyoutside)

  {
    // using vecgeom::GenericKernels;
    // here we are explicitely unrolling the loop since  a for statement will likely be a penality
    // check if second call to Abs is compiled away
    // and it can anyway not be vectorized
    /* rmax */
    typedef typename Backend::precision_v Float_t;
    typedef typename Backend::bool_v Bool_t;
    constexpr Float_t tol = 100. * vecgeom::kTolerance;

    //    // very fast check on z-height
    //    completelyoutside = point[2] > MakePlusTolerant<ForInside>( torus.rmax() );
    //    if (vecCore::EarlyReturnAllowed()) {
    //         if ( vecCore::MaskFull(completelyoutside) ) {
    //           return;
    //         }
    //    }

    Float_t rxy   = Sqrt(point[0] * point[0] + point[1] * point[1]);
    Float_t radsq = (rxy - torus.rtor()) * (rxy - torus.rtor()) + point[2] * point[2];

    if (ForInside) {
      completelyoutside = radsq > (tol * torus.rmax() + torus.rmax2()); // rmax
      completelyinside  = radsq < (-tol * torus.rmax() + torus.rmax2());
    } else {
      completelyoutside = radsq > torus.rmax2();
    }

    if (vecCore::EarlyReturnAllowed()) {
      if (vecCore::MaskFull(completelyoutside)) {
        return;
      }
    }
    /* rmin */
    if (ForInside) {
      completelyoutside |= radsq < (-tol * torus.rmin() + torus.rmin2()); // rmin
      completelyinside &= radsq > (tol * torus.rmin() + torus.rmin2());
    } else {
      completelyoutside |= radsq < torus.rmin2();
    }

    // NOT YET NEEDED WHEN NOT PHI TREATMENT
    if (vecCore::EarlyReturnAllowed()) {
      if (vecCore::MaskFull(completelyoutside)) {
        return;
      }
    }

    /* phi */
    if ((torus.dphi() < kTwoPi) && (notForDisk)) {
      Bool_t completelyoutsidephi;
      Bool_t completelyinsidephi;
      torus.GetWedge().GenericKernelForContainsAndInside<Backend, ForInside>(point, completelyinsidephi,
                                                                             completelyoutsidephi);

      completelyoutside |= completelyoutsidephi;
      if (ForInside) completelyinside &= completelyinsidephi;
    }
  }

  template <class Backend, bool notForDisk>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void ContainsKernel(UnplacedTorus2 const &torus, Vector3D<typename Backend::precision_v> const &point,
                             typename Backend::bool_v &inside)
  {
    typedef typename Backend::bool_v Bool_t;
    Bool_t unused;
    Bool_t outside;
    GenericKernelForContainsAndInside<Backend, false, notForDisk>(torus, point, unused, outside);
    inside = !outside;
  }
  // template <TranslationCode transCodeT, RotationCode rotCodeT>
  template <class Backend>
  VECGEOM_CUDA_HEADER_BOTH
  static void InsideKernel(UnplacedTorus2 const &torus, Vector3D<typename Backend::precision_v> const &point,
                           typename Backend::inside_v &inside)
  {

    typedef typename Backend::bool_v Bool_t;
    //
    Bool_t completelyinside, completelyoutside;
    GenericKernelForContainsAndInside<Backend, true, true>(torus, point, completelyinside, completelyoutside);
    inside = EInside::kSurface;
    vecCore::MaskedAssign(inside, completelyoutside, EInside::kOutside);
    vecCore::MaskedAssign(inside, completelyinside, EInside::kInside);
  }

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void UnplacedContains(UnplacedTorus2 const &torus, Vector3D<typename Backend::precision_v> const &point,
                               typename Backend::bool_v &inside)
  {
    ContainsKernel<Backend, true>(torus, point, inside);
  }
  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void UnplacedContainsDisk(UnplacedTorus2 const &torus, Vector3D<typename Backend::precision_v> const &point,
                                   typename Backend::bool_v &inside)
  {
    ContainsKernel<Backend, false>(torus, point, inside);
  }
  template <typename Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void Contains(UnplacedTorus2 const &unplaced, Transformation3D const &transformation,
                       Vector3D<typename Backend::precision_v> const &point,
                       Vector3D<typename Backend::precision_v> &localPoint, typename Backend::bool_v &inside)
  {
    localPoint = transformation.Transform<transCodeT, rotCodeT>(point);
    UnplacedContains<Backend>(unplaced, localPoint, inside);
  }
  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void Inside(UnplacedTorus2 const &torus, Transformation3D const &transformation,
                     Vector3D<typename Backend::precision_v> const &point, typename Backend::inside_v &inside)
  {
    InsideKernel<Backend>(torus, transformation.Transform<transCodeT, rotCodeT>(point), inside);
  }

  /////End GenericKernel Contains/Inside implementation

  template <class T>
  VECGEOM_CUDA_HEADER_BOTH
  static T CheckZero(T b, T c, T d, T e, T x)
  {
    T x2 = x * x;
    return x2 * x2 + b * x2 * x + c * x2 + d * x + e;
  }

  template <class T>
  VECGEOM_CUDA_HEADER_BOTH
  static T NewtonIter(T b, T c, T d, T /*e*/, T x, T fold)
  {
    T x2     = x * x;
    T fprime = 4 * x2 * x + 3 * b * x2 + 2 * c * x + d;
    return x - fold / fprime;
  }

  //_____________________________________________________________________________
  template <class T>
  VECGEOM_CUDA_HEADER_BOTH
  static T DistSqrToTorusR(UnplacedTorus2 const &torus, Vector3D<T> const &point, Vector3D<T> const &dir, T dist)
  {
    // Computes the squared distance to "axis" or "defining ring" of the torus from point point + t*dir;
    Vector3D<T> p = point + dir * dist;
    T rxy         = p.Perp();
    return (rxy - torus.rtor()) * (rxy - torus.rtor()) + p.z() * p.z();
  }

  template <class Backend, bool ForRmin>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static typename Backend::precision_v ToBoundary(UnplacedTorus2 const &torus,
                                                  Vector3D<typename Backend::precision_v> const &pt,
                                                  Vector3D<typename Backend::precision_v> const &dir, Precision radius,
                                                  bool out)
  {
    // to be taken from ROOT
    // Returns distance to the surface or the torus from a point, along
    // a direction. Point is close enough to the boundary so that the distance
    // to the torus is decreasing while moving along the given direction.
    typedef typename Backend::precision_v Real_v;

    // Compute coeficients of the quartic
    Real_v s             = vecgeom::kInfLength;
    constexpr Real_v tol = 100. * vecgeom::kTolerance;
    Real_v r0sq          = pt[0] * pt[0] + pt[1] * pt[1] + pt[2] * pt[2];
    Real_v rdotn         = pt[0] * dir[0] + pt[1] * dir[1] + pt[2] * dir[2];
    Real_v rsumsq        = torus.rtor2() + radius * radius;
    Real_v a             = 4. * rdotn;
    Real_v b             = 2. * (r0sq + 2. * rdotn * rdotn - rsumsq + 2. * torus.rtor2() * dir[2] * dir[2]);
    Real_v c             = 4. * (r0sq * rdotn - rsumsq * rdotn + 2. * torus.rtor2() * pt[2] * dir[2]);
    Real_v d             = r0sq * r0sq - 2. * r0sq * rsumsq + 4. * torus.rtor2() * pt[2] * pt[2] +
               (torus.rtor2() - radius * radius) * (torus.rtor2() - radius * radius);

    Real_v x[4] = {vecgeom::kInfLength, vecgeom::kInfLength, vecgeom::kInfLength, vecgeom::kInfLength};
    int nsol    = 0;

    // special condition
    if (Abs(dir[2]) < 1E-3 && Abs(pt[2]) < 0.1 * radius) {
      Real_v r0        = torus.rtor() - Sqrt((radius - pt[2]) * (radius + pt[2]));
      Real_v invdirxy2 = 1. / (1 - dir.z() * dir.z());
      Real_v b0        = (pt[0] * dir[0] + pt[1] * dir[1]) * invdirxy2;
      Real_v c0        = (pt[0] * pt[0] + (pt[1] - r0) * (pt[1] + r0)) * invdirxy2;
      Real_v delta     = b0 * b0 - c0;
      if (delta > 0) {
        x[nsol] = -b0 - Sqrt(delta);
        if (x[nsol] > -tol) nsol++;
        x[nsol] = -b0 + Sqrt(delta);
        if (x[nsol] > -tol) nsol++;
      }
      r0    = torus.rtor() + Sqrt((radius - pt[2]) * (radius + pt[2]));
      c0    = (pt[0] * pt[0] + (pt[1] - r0) * (pt[1] + r0)) * invdirxy2;
      delta = b0 * b0 - c0;
      if (delta > 0) {
        x[nsol] = -b0 - Sqrt(delta);
        if (x[nsol] > -tol) nsol++;
        x[nsol] = -b0 + Sqrt(delta);
        if (x[nsol] > -tol) nsol++;
      }
      if (nsol) {
        Sort4(x);
      }
    } else { // generic case
      nsol = SolveQuartic(a, b, c, d, x);
    }
    if (!nsol) return vecgeom::kInfLength;

    // look for first positive solution
    Real_v ndotd;
    bool inner = Abs(radius - torus.rmin()) < vecgeom::kTolerance;
    for (int i = 0; i < nsol; i++) {
      if (x[i] < -10) continue;

      Vector3D<Precision> r0   = pt + x[i] * dir;
      Vector3D<Precision> norm = r0;
      r0.z()                   = 0.;
      r0.Normalize();
      r0 *= torus.rtor();
      norm -= r0;
      // norm = pt
      // for (unsigned int ipt = 0; ipt < 3; ipt++)
      //   norm[ipt] = pt[ipt] + x[i] * dir[ipt] - r0[ipt];
      // ndotd = norm[0] * dir[0] + norm[1] * dir[1] + norm[2] * dir[2];
      ndotd = norm.Dot(dir);
      if (inner ^ out) {
        if (ndotd < 0) continue; // discard this solution
      } else {
        if (ndotd > 0) continue; // discard this solution
      }

      // The crossing point should be in the phi wedge
      if (torus.dphi() < vecgeom::kTwoPi) {
        if (!torus.GetWedge().ContainsWithBoundary<Backend>(r0)) continue;
      }

      s = x[i];
      // refine solution with Newton iterations
      Real_v eps   = vecgeom::kInfLength;
      Real_v delta = s * s * s * s + a * s * s * s + b * s * s + c * s + d;
      Real_v eps0  = -delta / (4. * s * s * s + 3. * a * s * s + 2. * b * s + c);
      int ntry     = 0;
      while (Abs(eps) > vecgeom::kTolerance) {
        if (Abs(eps0) > 100) break;
        s += eps0;
        if (Abs(s + eps0) < vecgeom::kTolerance) break;
        delta = s * s * s * s + a * s * s * s + b * s * s + c * s + d;
        eps   = -delta / (4. * s * s * s + 3. * a * s * s + 2. * b * s + c);
        if (Abs(eps) >= Abs(eps0)) break;
        ntry++;
        // Avoid infinite recursion
        if (ntry > 100) break;
        eps0 = eps;
      }
      // discard this solution
      if (s < -tol) continue;
      return Max(0., s);
    }
    return vecgeom::kInfLength;
  }

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void DistanceToIn(UnplacedTorus2 const &torus, Transformation3D const &transformation,
                           Vector3D<typename Backend::precision_v> const &point,
                           Vector3D<typename Backend::precision_v> const &direction,
                           typename Backend::precision_v const &stepMax, typename Backend::precision_v &distance)
  {

    typedef typename Backend::precision_v Float_t;
    typedef typename Backend::bool_v Bool_t;

    Vector3D<Float_t> localPoint     = transformation.Transform<transCodeT, rotCodeT>(point);
    Vector3D<Float_t> localDirection = transformation.TransformDirection<rotCodeT>(direction);

    ////////First naive implementation
    distance = kInfLength;

    // Check Bounding Cylinder first
    Bool_t inBounds;
    Bool_t done                       = false;
    typename Backend::inside_v inside = EInside::kOutside;
    Float_t tubeDistance              = kInfLength;

#ifndef VECGEOM_NO_SPECIALIZATION
    // call the tube functionality -- first of all we check whether we are inside
    // bounding volume
    TubeImplementation<TubeTypes::HollowTube>::Contains(torus.GetBoundingTube().GetStruct(), localPoint, inBounds);

    // only need to do this check if all particles (in vector) are outside ( otherwise useless )
    TubeImplementation<TubeTypes::HollowTube>::DistanceToIn(torus.GetBoundingTube().GetStruct(), localPoint,
                                                            localDirection, stepMax, tubeDistance);
#else
    // call the tube functionality -- first of all we check whether we are inside
    // bounding volume
    TubeImplementation<TubeTypes::UniversalTube>::Contains(torus.GetBoundingTube().GetStruct(), localPoint, inBounds);

    // only need to do this check if all particles (in vector) are outside ( otherwise useless )
    if (!inBounds)
      TubeImplementation<TubeTypes::UniversalTube>::DistanceToIn(torus.GetBoundingTube().GetStruct(), localPoint,
                                                                 localDirection, stepMax, tubeDistance);
    else
      tubeDistance = 0.;
#endif // VECGEOM_NO_SPECIALIZATION
    if (inBounds) {
      // Check points on the wrong side (inside torus)
      TorusImplementation2::InsideKernel<Backend>(torus, point, inside);
      if (inside == EInside::kInside) {
        done     = true;
        distance = -1.;
      }
    } else {
      done = (tubeDistance == kInfLength);
    }

    if (vecCore::EarlyReturnAllowed()) {
      if (vecCore::MaskFull(done)) {
        return;
      }
    }

    // Propagate the point to the bounding tube, as this will reduce the
    // coefficients of the quartic and improve precision of the solutions
    localPoint += tubeDistance * localDirection;
    Bool_t hasphi = (torus.dphi() < vecgeom::kTwoPi);
    if (hasphi) {
      Float_t d1, d2;

      auto wedge = torus.GetWedge();
      // checking distance to phi wedges
      // NOTE: if the tube told me its hitting surface, this would be unnessecary
      wedge.DistanceToIn<Backend>(localPoint, localDirection, d1, d2);

      // check phi intersections if bounding tube intersection is due to phi in which case we are done
      if (d1 != kInfLength) {
        Precision daxis = DistSqrToTorusR(torus, localPoint, localDirection, d1);
        if (daxis >= torus.rmin2() && daxis < torus.rmax2()) {
          distance = d1;
          // check if tube intersections is due to phi in which case we are done
          if (Abs(distance) < kTolerance) {
            distance += tubeDistance;
            return;
          }
        }
      }

      if (d2 != kInfLength) {
        Precision daxis = DistSqrToTorusR(torus, localPoint, localDirection, d2);
        if (daxis >= torus.rmin2() && daxis < torus.rmax2()) {
          distance = Min(d2, distance);
          // check if tube intersections is due to phi in which case we are done
          if (Abs(distance) < kTolerance) {
            distance += tubeDistance;
            return;
          }
        }
      }
      distance = kInfLength;
    }

    Float_t dd = ToBoundary<Backend, false>(torus, localPoint, localDirection, torus.rmax(), false);

    // in case of a phi opening we also need to check the Rmin surface
    if (torus.rmin() > 0.) {
      Float_t ddrmin = ToBoundary<Backend, true>(torus, localPoint, localDirection, torus.rmin(), false);
      dd             = Min(dd, ddrmin);
    }
    distance = Min(distance, dd);
    distance += tubeDistance;
    // This has to be added because distance can become > kInfLength due to
    // missing early returns in CUDA. This makes comparisons to kInfLength fail.
    vecCore::MaskedAssign(distance, distance > kInfLength, kInfLength);
    return;
  }

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void DistanceToOut(UnplacedTorus2 const &torus, Vector3D<typename Backend::precision_v> const &point,
                            Vector3D<typename Backend::precision_v> const &dir,
                            typename Backend::precision_v const & /*stepMax*/, typename Backend::precision_v &distance)
  {

    typedef typename Backend::precision_v Float_t;
    typedef typename Backend::bool_v Bool_t;
    distance = kInfLength;

    bool hasphi  = (torus.dphi() < kTwoPi);
    bool hasrmin = (torus.rmin() > 0);

    // Check points on the wrong side (inside torus)
    typename Backend::inside_v inside;
    TorusImplementation2::InsideKernel<Backend>(torus, point, inside);
    if (inside == EInside::kOutside) {
      distance = -1.;
      return;
    }

    Float_t dout = ToBoundary<Backend, false>(torus, point, dir, torus.rmax(), true);
    Float_t din(kInfLength);
    if (hasrmin) {
      din = ToBoundary<Backend, true>(torus, point, dir, torus.rmin(), true);
    }
    distance = Min(dout, din);

    if (hasphi) {
      Float_t distPhi1;
      Float_t distPhi2;
      torus.GetWedge().DistanceToOut<Backend>(point, dir, distPhi1, distPhi2);
      Bool_t smallerphi = distPhi1 < distance;
      if (!vecCore::MaskEmpty(smallerphi)) {
        Vector3D<Float_t> intersectionPoint = point + dir * distPhi1;
        Bool_t insideDisk;
        UnplacedContainsDisk<Backend>(torus, intersectionPoint, insideDisk);

        if (!vecCore::MaskEmpty(insideDisk)) // Inside Disk
        {
          Float_t diri = intersectionPoint.x() * torus.GetWedge().GetAlong1().x() +
                         intersectionPoint.y() * torus.GetWedge().GetAlong1().y();
          Bool_t rightside = (diri >= 0);

          vecCore::MaskedAssign(distance, rightside && smallerphi && insideDisk, distPhi1);
        }
      }
      smallerphi = distPhi2 < distance;
      if (!vecCore::MaskEmpty(smallerphi)) {

        Vector3D<Float_t> intersectionPoint = point + dir * distPhi2;
        Bool_t insideDisk;
        UnplacedContainsDisk<Backend>(torus, intersectionPoint, insideDisk);
        if (!vecCore::MaskEmpty(insideDisk)) // Inside Disk
        {
          Float_t diri2 = intersectionPoint.x() * torus.GetWedge().GetAlong2().x() +
                          intersectionPoint.y() * torus.GetWedge().GetAlong2().y();
          Bool_t rightside = (diri2 >= 0);
          vecCore::MaskedAssign(distance, rightside && (distPhi2 < distance) && smallerphi && insideDisk, distPhi2);
        }
      }
    }
    if (distance >= kInfLength) distance                             = -1.;
    if (vecCore::math::Abs(distance) < vecgeom::kTolerance) distance = 0.;
  }

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void SafetyToIn(UnplacedTorus2 const &torus, Transformation3D const &transformation,
                         Vector3D<typename Backend::precision_v> const &point, typename Backend::precision_v &safety)
  {

    typedef typename Backend::precision_v Float_t;
    Vector3D<Float_t> localPoint = transformation.Transform<transCodeT, rotCodeT>(point);

    // implementation taken from TGeoTorus
    Float_t rxy = Sqrt(localPoint[0] * localPoint[0] + localPoint[1] * localPoint[1]);
    Float_t rad = Sqrt((rxy - torus.rtor()) * (rxy - torus.rtor()) + localPoint[2] * localPoint[2]);
    safety      = rad - torus.rmax();
    if (torus.rmin()) {
      safety = Max(torus.rmin() - rad, rad - torus.rmax());
    }

    bool hasphi = (torus.dphi() < kTwoPi);
    if (hasphi && (rxy != 0.)) {
      Float_t safetyPhi = torus.GetWedge().SafetyToIn<Backend>(localPoint);
      safety            = Max(safetyPhi, safety);
    }
  }

  template <class Backend>
  VECGEOM_FORCE_INLINE
  VECGEOM_CUDA_HEADER_BOTH
  static void SafetyToOut(UnplacedTorus2 const &torus, Vector3D<typename Backend::precision_v> const &point,
                          typename Backend::precision_v &safety)
  {

    typedef typename Backend::precision_v Float_t;
    Float_t rxy = Sqrt(point[0] * point[0] + point[1] * point[1]);
    Float_t rad = Sqrt((rxy - torus.rtor()) * (rxy - torus.rtor()) + point[2] * point[2]);
    safety      = torus.rmax() - rad;
    if (torus.rmin()) {
      safety = Min(rad - torus.rmin(), torus.rmax() - rad);
    }

    // TODO: extend implementation for phi sector case
    bool hasphi = (torus.dphi() < kTwoPi);
    if (hasphi) {
      Float_t safetyPhi = torus.GetWedge().SafetyToOut<Backend>(point);
      safety            = Min(safetyPhi, safety);
    }
  }
}; // end struct
}
} // end namespace

#endif // VECGEOM_VOLUMES_KERNEL_TORUSIMPLEMENTATION_H_
