
/// @file ScaledShape.h
/// @author Mihaela Gheata (mihaela.gheata@cern.ch)
//
/// Includes all headers related to the tube volume

#ifndef VECGEOM_VOLUMES_SCALEDSHAPE_H_
#define VECGEOM_VOLUMES_SCALEDSHAPE_H_

#include "base/Global.h"
#include "volumes/PlacedScaledShape.h"
#include "volumes/SpecializedScaledShape.h"
#include "volumes/UnplacedScaledShape.h"

#endif // VECGEOM_VOLUMES_SCALEDSHAPE_H_
