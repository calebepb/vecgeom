//
// ********************************************************************
// * This Software is part of the AIDA Unified Solids Library package *
// * See: https://aidasoft.web.cern.ch/USolids                        *
// ********************************************************************
//
// $Id:$
//
// --------------------------------------------------------------------
//
// UGenericTrap
//
// Class description:
//
// UGenericTrap is a solid which represents an arbitrary trapezoid with
// up to 8 vertices standing on two parallel planes perpendicular to Z axis.
//
// Parameters in the constructor:
// - name               - solid name
// - halfZ              - the solid half length in Z
// - vertices           - the (x,y) coordinates of vertices:
//                        o first four points: vertices[i], i<4
//                          are the vertices sitting on the -halfZ plane;
//                        o last four points: vertices[i], i>=4
//                          are the vertices sitting on the +halfZ plane.
//
//   The order of defining the vertices of the solid is the following:
//      - point 0 is connected with points 1,3,4
//      - point 1 is connected with points 0,2,5
//      - point 2 is connected with points 1,3,6
//      - point 3 is connected with points 0,2,7
//      - point 4 is connected with points 0,5,7
//      - point 5 is connected with points 1,4,6
//      - point 6 is connected with points 2,5,7
//      - point 7 is connected with points 3,4,6
// Points can be identical in order to create shapes with less than
// 8 vertices.
//
// 21.10.13 Tatiana Nikitina, CERN; Ivana Hrivnacova, IPN Orsay
//          Adapted from Root Arb8 implementation
// --------------------------------------------------------------------

#ifndef USOLIDS_UGenericTrap_HH
#define USOLIDS_UGenericTrap_HH

#include "UVector2.hh"
#include <iomanip>

#ifdef VECGEOM_REPLACE_USOLIDS

//============== here for VecGeom-based implementation

//#include "volumes/SpecializedGenTrap.h"
#include "volumes/LogicalVolume.h"
#include "volumes/UnplacedGenTrap.h"
#include "base/Vector3D.h"
#include "base/Transformation3D.h"

#include "volumes/USolidsAdapter.h"

class UGenericTrap : public vecgeom::USolidsAdapter<vecgeom::UnplacedGenTrap> {

  // just forwards UGenericTrap to vecgeom generic trapezoid
  using Shape_t = vecgeom::UnplacedGenTrap;
  using Base_t  = vecgeom::USolidsAdapter<vecgeom::UnplacedGenTrap>;

  // inherit all constructors
  using Base_t::Base_t;

public:
  // Accessors which are available in UnplacedGenTrap
  UGenericTrap() : Base_t("") {}

  inline double GetZHalfLength() const { return GetDZ(); }
  inline void SetZHalfLength(double dz) { SetDZ(dz); }
  inline int GetNofVertices() const { return 8; }
  inline UVector2 GetVertex(int index) const { return (UVector2(GetVerticesX()[index], GetVerticesY()[index])); }
  inline const std::vector<UVector2> &GetVertices() const { return fVertices; }
  inline double GetTwistAngle(int i) const { return GetTwist(i); }
  inline bool IsTwisted() const { return (!IsPlanar()); }
  inline int GetVisSubdivisions() const { return 0; }
  inline void SetVisSubdivisions(int) {}
  inline UVector3 GetMinimumBBox() const
  {
    vecgeom::Vector3D<double> amin, amax;
    Extent(amin, amax);
    return (UVector3(amin[0], amin[1], amin[2]));
  }
  inline UVector3 GetMaximumBBox() const
  {
    vecgeom::Vector3D<double> amin, amax;
    Extent(amin, amax);
    return (UVector3(amax[0], amax[1], amax[2]));
  }
  void Initialise(const std::vector<UVector2> &v)
  {
    // Initialize from data of UnplacedGenTrap
    double verticesx[8], verticesy[8];
    for (int i = 0; i < 8; ++i) {
      fVertices.push_back(v[i]);
      verticesx[i] = v[i].x;
      verticesy[i] = v[i].y;
    }
    Initialize(verticesx, verticesy, GetDZ());
  }

  // o provide a new object which is a clone of the solid
  VUSolid *Clone() const override
  {
    return new UGenericTrap(GetName().c_str(), GetVerticesX(), GetVerticesY(), GetDZ());
  }

  UGeometryType GetEntityType() const override { return "UGenTrap"; }
  void ComputeBBox(UBBox * /*aBox*/, bool /*aStore = false*/) override {}
  inline void GetParametersList(int /*aNumber*/, double * /*aArray*/) const override {}

  std::ostream &StreamInfo(std::ostream &os) const override
  {
    int oldprc = os.precision(16);
    os << "-----------------------------------------------------------\n"
       << "    *** Dump for solid - " << GetEntityType() << " *** \n"
       << "    =================================================== \n"
       << " Solid geometry type: Generic trapezoid adapter\n"
       << "   half length Z: " << GetDZ() << " mm \n"
       << "   list of vertices:\n";

    for (int i = 0; i < 8; ++i) {
      os << std::setw(5) << "#" << i << "   vx = " << GetVerticesX()[i] << " mm"
         << "   vy = " << GetVerticesY()[i] << " mm" << std::endl;
    }
    os.precision(oldprc);
    return os;
  }

private:
  // Data members required to have USolids compatibility
  std::vector<UVector2> fVertices;
};

//============== end of VecGeom-based implementation

#else

//============== here for USolids-based implementation
#ifndef USOLIDS_VUSolid
#include "VUSolid.hh"
#endif

#ifndef USOLIDS_UUtils
#include "UUtils.hh"
#endif

#include <vector>

class VUFacet;
class UTessellatedSolid;
class UBox;

class UGenericTrap : public VUSolid {
public: // with description
  UGenericTrap(const std::string &name, double halfZ, const std::vector<UVector2> &vertices);
  // Constructor

  ~UGenericTrap();
  // Destructor

  // Accessors

  inline double GetZHalfLength() const;
  inline void SetZHalfLength(double);
  inline int GetNofVertices() const;
  inline UVector2 GetVertex(int index) const;
  inline const std::vector<UVector2> &GetVertices() const;
  inline double GetTwistAngle(int index) const;
  inline bool IsTwisted() const;
  inline int GetVisSubdivisions() const;
  inline void SetVisSubdivisions(int subdiv);

  // Solid methods

  EnumInside Inside(const UVector3 &aPoint) const;
  bool Normal(const UVector3 &aPoint, UVector3 &aNormal) const;
  double SafetyFromInside(const UVector3 &aPoint, bool aAccurate = false) const;
  double SafetyFromOutside(const UVector3 &aPoint, bool aAccurate = false) const;
  double DistanceToIn(const UVector3 &aPoint, const UVector3 &aDirection, double aPstep = UUtils::kInfinity) const;

  double DistanceToOut(const UVector3 &aPoint, const UVector3 &aDirection, UVector3 &aNormalVector, bool &aConvex,
                       double aPstep = UUtils::kInfinity) const;
  void Extent(UVector3 &aMin, UVector3 &aMax) const;
  double Capacity();
  double SurfaceArea();
  VUSolid *Clone() const;

  inline UGeometryType GetEntityType() const { return "GenericTrap"; }
  inline void ComputeBBox(UBBox * /*aBox*/, bool /*aStore = false*/) {}
  inline void GetParametersList(int /*aNumber*/, double * /*aArray*/) const {}

  UVector3 GetPointOnSurface() const;

  std::ostream &StreamInfo(std::ostream &os) const;

public:
  UGenericTrap();
  // Fake default constructor for usage restricted to direct object
  // persistency for clients requiring preallocation of memory for
  // persistifiable objects.

  UGenericTrap(const UGenericTrap &rhs);
  UGenericTrap &operator=(const UGenericTrap &rhs);
  // Copy constructor and assignment operator.

  void Initialise(const std::vector<UVector2> &vertices);
  inline UVector3 GetMinimumBBox() const;
  inline UVector3 GetMaximumBBox() const;

private:
  // Internal methods

  inline void SetTwistAngle(int index, double twist);
  bool ComputeIsTwisted();
  bool CheckOrder(const std::vector<UVector2> &vertices) const;
  bool IsSegCrossing(const UVector2 &a, const UVector2 &b, const UVector2 &c, const UVector2 &d) const;
  bool IsSegCrossingZ(const UVector2 &a, const UVector2 &b, const UVector2 &c, const UVector2 &d) const;
  bool IsSameLineSegment(const UVector2 &p, const UVector2 &l1, const UVector2 &l2) const;
  bool IsSameLine(const UVector2 &p, const UVector2 &l1, const UVector2 &l2) const;

  void ReorderVertices(std::vector<UVector3> &vertices) const;
  void ComputeBBox();

  VUFacet *MakeDownFacet(const std::vector<UVector3> &fromVertices, int ind1, int ind2, int ind3) const;
  VUFacet *MakeUpFacet(const std::vector<UVector3> &fromVertices, int ind1, int ind2, int ind3) const;
  VUFacet *MakeSideFacet(const UVector3 &downVertex0, const UVector3 &downVertex1, const UVector3 &upVertex1,
                         const UVector3 &upVertex0) const;
  UTessellatedSolid *CreateTessellatedSolid() const;

  EnumInside InsidePolygone(const UVector3 &p, const UVector2 *poly) const;
  double DistToPlane(const UVector3 &p, const UVector3 &v, const int ipl) const;
  double DistToTriangle(const UVector3 &p, const UVector3 &v, const int ipl) const;
  UVector3 NormalToPlane(const UVector3 &p, const int ipl) const;
  double SafetyToFace(const UVector3 &p, const int iseg) const;
  double GetFaceSurfaceArea(const UVector3 &p0, const UVector3 &p1, const UVector3 &p2, const UVector3 &p3) const;

private:
  // static data members

  static const int fgkNofVertices;
  static const double fgkTolerance;

  // data members

  double fDz;
  std::vector<UVector2> fVertices;
  bool fIsTwisted;
  double fTwist[4];
  UTessellatedSolid *fTessellatedSolid;
  UVector3 fMinBBoxVector;
  UVector3 fMaxBBoxVector;
  int fVisSubdivisions;
  UBox *fBoundBox;

  enum ESide { kUndefined, kXY0, kXY1, kXY2, kXY3, kMZ, kPZ };
  // Codes for faces (kXY[num]=num of lateral face,kMZ= minus z face etc)

  double fSurfaceArea;
  double fCubicVolume;
  // Surface and Volume
};

#include "UGenericTrap.icc"
//============== end of USolids-based implementation

#endif // VECGEOM_REPLACE_USOLIDS
#endif // USOLIDS_UGenericTrap_HH
