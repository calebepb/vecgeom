// This files was autogenerated by vecgeom::Nuclei::ReadFile

#include "materials/Nucleus.h"
namespace vecgeom {
inline namespace VECGEOM_IMPL_NAMESPACE {

VECGEOM_CUDA_HEADER_BOTH
void CreateNuclei0000();
VECGEOM_CUDA_HEADER_BOTH
void CreateNuclei0001();
VECGEOM_CUDA_HEADER_BOTH
void CreateNuclei0002();

#ifdef VECGEOM_NVCC
VECGEOM_CUDA_HEADER_DEVICE bool fgCreateNucleiInitDoneDev = false;
#endif

//________________________________________________________________________________
VECGEOM_CUDA_HEADER_BOTH
void Nucleus::CreateNuclei()
{
#ifndef VECGEOM_NVCC
  static bool fgCreateNucleiInitDone = false;
#else
  bool &fgCreateNucleiInitDone(fgCreateNucleiInitDoneDev);
#endif
  if (fgCreateNucleiInitDone) return;
  fgCreateNucleiInitDone = true;
  CreateNuclei0000();
  CreateNuclei0001();
  CreateNuclei0002();
} // End of CreateNuclei
} // End of inline namespace
} // End of vecgeom namespace
#if defined(__clang__) && !defined(__APPLE__)
#pragma clang optimize on
#endif
