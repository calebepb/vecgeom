/// \file Vector.h
/// \author Johannes de Fine Licht (johannes.definelicht@cern.ch)

#ifndef VECGEOM_BASE_VECTOR_H_
#define VECGEOM_BASE_VECTOR_H_

#include "base/Global.h"
#include <initializer_list>
#ifdef VECGEOM_CUDA
#include "backend/cuda/Interface.h"
#endif

namespace vecgeom {

VECGEOM_DEVICE_FORWARD_DECLARE(template <typename Type> class Vector;);

inline namespace VECGEOM_IMPL_NAMESPACE {

template <typename Type>
class Vector {

private:
  Type *fData;
  size_t fSize, fMemorySize;
  bool fAllocated;

public:
  VECGEOM_CUDA_HEADER_BOTH
  Vector() : fData(new Type[1]), fSize(0), fMemorySize(1), fAllocated(true) {}

  VECGEOM_CUDA_HEADER_BOTH
  Vector(const int maxsize) : fData(new Type[maxsize]), fSize(0), fMemorySize(maxsize), fAllocated(true) {}

  VECGEOM_CUDA_HEADER_BOTH
  Vector(Type *const vec, const int sz) : fData(vec), fSize(sz), fMemorySize(sz), fAllocated(false) {}

  VECGEOM_CUDA_HEADER_BOTH
  Vector(Type *const vec, const int sz, const int maxsize)
      : fData(vec), fSize(sz), fMemorySize(maxsize), fAllocated(false)
  {
  }

  VECGEOM_CUDA_HEADER_BOTH
  Vector(Vector const &other) : fSize(other.fSize), fMemorySize(other.fMemorySize), fAllocated(true)
  {
    fData = new Type[fMemorySize];
    for (size_t i = 0; i < fSize; ++i)
      fData[i]    = other.fData[i];
  }

  VECGEOM_CUDA_HEADER_BOTH
  Vector &operator=(Vector const &other)
  {
    if (&other != this) {
      fSize       = other.fSize;
      fMemorySize = other.fMemorySize;
      fAllocated  = other.fAllocated;
      if (fAllocated) {
        if (fMemorySize > 0) {
          delete[] fData;
          fData = new Type[fMemorySize];
          for (size_t i = 0; i < fSize; ++i)
            fData[i]    = other.fData[i];
        } else {
          fData = nullptr;
        }
      } else
        fData = other.fData;
    }
    return *this;
  }

  VECGEOM_CUDA_HEADER_BOTH
  Vector(std::initializer_list<Type> entries)
  {
    fSize       = entries.size();
    fData       = new Type[fSize];
    fAllocated  = true;
    fMemorySize = entries.size() * sizeof(Type);
    for (auto itm : entries)
      this->push_back(itm);
  }

  VECGEOM_CUDA_HEADER_BOTH
  ~Vector()
  {
    if (fAllocated) delete[] fData;
  }

  VECGEOM_CUDA_HEADER_BOTH
  void clear()
  {
    if (fAllocated) {
      delete[] fData;
      fData = new Type[fMemorySize];
    }
    fSize = 0;
  }

  VECGEOM_CUDA_HEADER_BOTH
  VECGEOM_FORCE_INLINE
  Type &operator[](const int index) { return fData[index]; }

  VECGEOM_CUDA_HEADER_BOTH
  VECGEOM_FORCE_INLINE
  Type const &operator[](const int index) const { return fData[index]; }

  VECGEOM_CUDA_HEADER_BOTH
  void push_back(const Type item)
  {
    if (fSize == fMemorySize) {
      assert(fAllocated && "Trying to push on a 'fixed' size vector (memory "
                           "not allocated by Vector itself)");
      fMemorySize    = fMemorySize << 1;
      Type *fDataNew = new Type[fMemorySize];
      for (size_t i = 0; i < fSize; ++i)
        fDataNew[i] = fData[i];
      delete[] fData;
      fData = fDataNew;
    }
    fData[fSize] = item;
    fSize++;
  }

  typedef Type *iterator;
  typedef Type const *const_iterator;

  VECGEOM_CUDA_HEADER_BOTH
  VECGEOM_FORCE_INLINE
  iterator begin() const { return &fData[0]; }

  VECGEOM_CUDA_HEADER_BOTH
  VECGEOM_FORCE_INLINE
  iterator end() const { return &fData[fSize]; }

  VECGEOM_CUDA_HEADER_BOTH
  VECGEOM_FORCE_INLINE
  const_iterator cbegin() const { return &fData[0]; }

  VECGEOM_CUDA_HEADER_BOTH
  VECGEOM_FORCE_INLINE
  const_iterator cend() const { return &fData[fSize]; }

  VECGEOM_CUDA_HEADER_BOTH
  VECGEOM_FORCE_INLINE
  size_t size() const { return fSize; }

  VECGEOM_CUDA_HEADER_BOTH
  VECGEOM_FORCE_INLINE
  void resize(size_t newsize, Type value)
  {
    Type *temp = new Type[newsize];
    if (newsize <= fSize) {
      for (size_t i = 0; i < newsize; ++i)
        temp[i]     = fData[i];
      delete[] fData;
      fData = new Type[newsize];
      fSize = newsize;
      for (size_t i = 0; i < newsize; ++i)
        fData[i]    = temp[i];
    } else {
      for (size_t i = 0; i < fSize; ++i)
        temp[i]     = fData[i];
      delete[] fData;
      fData = new Type[newsize];
      for (size_t i = 0; i < fSize; ++i)
        fData[i]    = temp[i];
      for (size_t i = fSize; i < newsize; ++i)
        fData[i]    = value;
    }
    fSize = newsize;
    delete[] temp;
  }
#ifdef VECGEOM_CUDA_INTERFACE
  DevicePtr<cuda::Vector<CudaType_t<Type>>> CopyToGpu(DevicePtr<CudaType_t<Type>> const gpu_ptr_arr,
                                                      DevicePtr<cuda::Vector<CudaType_t<Type>>> const gpu_ptr) const
  {
    gpu_ptr.Construct(gpu_ptr_arr, size());
    return gpu_ptr;
  }
#endif

private:
};
}
} // End global namespace

#endif // VECGEOM_BASE_CONTAINER_H_
